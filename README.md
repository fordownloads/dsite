### OrangeFox Recovery
- [Wiki](https://wiki.orangefox.tech/)
- [File Downloads](https://files.orangefox.download/)
- [Website Downloads](https://orangefox.download/)
- [Crowdin Translations](https://orangefox.crowdin.com/u/projects/8)

### Telegram 
- [Support](https://t.me/OrangeFoxChat)
- [News](https://t.me/OrangeFoxNEWS)
