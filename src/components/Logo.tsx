import React from 'react';
import { ReactComponent as LogoSvg } from '../arts/logo.svg'

const Logo: React.SFC = () => {
    return (
        <LogoSvg className="logo" />
    );
}

export { Logo };
